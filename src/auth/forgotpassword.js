import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  View,
  TextInput,
  TouchableOpacity,
  ImageBackground,
  Image,
  Alert
} from 'react-native';
import { Container,Text, Header,Button, Content, Item, Input, Icon ,Label,Card,Left,List, ListItem,Form, Body, Right} from 'native-base';
import firebase from 'firebase';
import store from 'react-native-simple-store';


export default class forgotpassword extends Component{
  static navigationOptions = {
      headerStyle: {
        position: 'absolute',
        top: 0,
        left: 0
      },
      headerBackTitleStyle: {
          opacity: 0,
      },
      headerTintColor: '#fff'
  };

  constructor(props){
  super(props);
  this.state = {
    email:'',
    password:'',
    loading: true,
    key:true,
  }
}

// componentDidMount(){
//   // const { navigate } = this.props.navigation;
//   store.get('email').then((email) =>{
//     console.log(email)
//   if(email){
//     // this.props.navigation.navigate('Home')}
//   })
// }

  render() {
      return (
      <Container style={styles.container}>
          <Text style={styles.header}> SPA </Text>
            <Item regular style={{marginBottom:20,backgroundColor:'#ffffff95',borderRadius:100}}>
              <Image style={{marginLeft:20}} source={require('./image/user.png')}/>
              <Input
                placeholder='Email'
                style={{color:'#424242'}}     
                onChangeText={(email)=> this.setState({email})} 
                returnKeyType='next'
                underlineColorAndroid='transparent'/>
            </Item>

            <Item regular style={{marginBottom:20,backgroundColor:'#ffffff95',borderRadius:100}} >
                <Image style={{marginLeft:20}} source={require('./image/password.png')}/>
                <Input
                  placeholder='Password'
                  style={{color:'#424242'}}  
                  onChangeText={(password)=> this.setState({password})} 
                  returnKeyType='next'
                  secureTextEntry={this.state.key}
                  underlineColorAndroid='transparent'/>
                  <TouchableOpacity onPressIn={ () => this.key() } onPressOut={ () => this.key1() } >
                    <Image style={{width:20,height:20,marginRight:10}} source={require('./image/eye.png')}/>
                  </TouchableOpacity>
              </Item>
              
          <TouchableOpacity
            style={styles.btn}
            onPress={this.onLogin}>
            <Text style={{color:'#424242',fontSize:15,fontWeight:'bold',}}> MASUK </Text>
          </TouchableOpacity>
          
          <TouchableOpacity
            style={styles.btn}
            onPress={this.daftar}>
            <Text style={{color:'#424242',fontSize:15,fontWeight:'bold',}}> DAFTAR </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.btn}
            onPress={this.onLogin}>
            <Text style={{color:'#424242',fontSize:15,fontWeight:'bold',}}> LUPA PASSWORD </Text>
          </TouchableOpacity>
        </Container>
      )
    }

    key = ()=>{
    this.setState({ 
      key:false ,
    })}

    key1 = ()=>{
    this.setState({ 
      key:true ,
    })} 

    daftar = ()=>{
      this.props.navigation.navigate('register');
    }

    onLogin = () => {
      const { email, password } = this.state;
      firebase.auth().signInWithEmailAndPassword(email, password)
        .then((response) => {
          if (response.user.email === email){
          store.save('email', response.email)
          // this.props.navigation.navigate('Home');
          Alert.alert(
            'Selamat Datang',
            'SPA Solution'
          )
          }
        })
        .catch((error) => {
         Alert.alert(
            'Tidak dapat login',
            'Cek kembali email dan password anda'
          )
        });
    }
  }

  const styles = StyleSheet.create({
  wrapper:{
    flex:1,
  },
  container:{
    flex:1,
    alignItems:'center',
    justifyContent:'center',
    paddingLeft:60,
    paddingRight:60,
    backgroundColor: '#eeeeee',
  },
  header:{
    fontSize:40,
    marginBottom:60,
    color:'#424242',
    fontWeight:'bold',
  },
  textInput:{
    alignSelf: 'stretch',
    padding:16,
    marginBottom:20,
    backgroundColor:'#ffffff99',
    borderRadius:20,
  },
  btn:{
    alignSelf:'stretch',
    backgroundColor:'#33691e95',
    padding:15,
    alignItems:'center',
    borderRadius:10,
    marginVertical: 5,
  }
})
