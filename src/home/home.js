import React, { Component } from 'react';
import { AppRegistry,BackHandler,View,StyleSheet,ListView,Image,ImageBackground,TouchableOpacity } from 'react-native';
import { StackNavigator,DrawerNavigator,TabNavigator,TabBarBottom  } from 'react-navigation';
import { Container, Header, Content, Item,Separator,Thumbnail,Text, Input,Button, Icon ,Label,Card,CardItem,Left,List,Badge, ListItem,Form, Body, Right} from 'native-base';
import Carousel from 'react-native-banner-carousel';
import store from 'react-native-simple-store';
var Spinner = require('react-native-spinkit');
import firebase from 'firebase';
import moment from 'moment'

export default class home extends Component {
   static navigationOptions = {
    headerStyle: {
          position: 'absolute',
          top: 0,
          left: 0
        },
      tabBarLabel: 'Home',
      tabBarIcon: ({ tintColor }) => (
        <Image
          source={require('./image/home.png')}
          style={[styles.icon, { tintColor: tintColor }]}
        />
      ),
    };

    componentDidMount(){
      const { navigate } = this.props.navigation;
      store.get('uid').then((res) =>{
        console.log(res)
      fetch('https://spa-padang.firebaseio.com/user/' + res + '.json?print=pretty')
        .then((response) => response.json())
        .then((responseJson) =>{
          store.save('name',responseJson.nama_lengkap)
          store.save('alamat',responseJson.alamat)
          store.save('nohp',responseJson.nohp)
          this.setState({ dataSource: responseJson, loading: false }) 
          })
        })
  }
  

  render() {
    return (
      <View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  icon: {
    width:20,height:20
  },
});
