import React, { Component } from 'react';
import { AppRegistry,View,StyleSheet,ListView,Image,TouchableOpacity } from 'react-native';
import { Container,Text, Header, Content, Item,Separator, Input,Button, Icon ,Label,Card,CardItem,Left,List,Badge, ListItem,Form, Body, Right} from 'native-base';
import store from 'react-native-simple-store';
import moment from 'moment'
var Spinner = require('react-native-spinkit');

export default class merk_mobil extends Component{

  constructor(){
    super();
    this.state={
      pesan :'',
      dataSource: new ListView.DataSource({rowHasChanged:(r1,r2)=> r1!=r2}),
      loading:true,
    }  
  }

  componentDidMount() {
    this.fetchMessage()
    this.timer = setTimeout(() => this.componentDidMount(), 5000)
  }

  fetchMessage = () => {
      console.reportErrorsAsExceptions = false;
      fetch('https://spa-padang.firebaseio.com/merek_mobil.json?print=pretty')
      .then((response) => response.json())
      .then((responseJson) =>{
        data = responseJson; // here we have all products data
        this.setState({
          dataSource: this.state.dataSource.cloneWithRows(data),
          data:responseJson
        })
      })
      .catch((error) =>{
        console.error(error);
      })
  }

  render(){
    const { dataSource,loading} = this.state;
    const { params } = this.props.navigation.state;
    const { navigate } = this.props.navigation.goBack;

    return(
      <Container style={{backgroundColor:'#E0E0E0'}}>
        <Header transparent style={{backgroundColor: '#828282'}}>
          <Body>
              <Text style={{textAlignVertical:'center',fontSize:20,color: '#E0E0E0',paddingVertical:5,paddingLeft: 10,fontWeight: "normal",alignItems: 'flex-end',fontFamily:'Quicksand-Bold'}}>Transaksi </Text>
          </Body>
        </Header>
        <Content>
        <ListView
                  horizontal={false}
                  style={styles.body}
                  dataSource={this.state.dataSource}
                  enableEmptySections={true}
                  renderRow={(rowData)=>
                  <TouchableOpacity style={{ margin: 2 }} onPress={
                                                                    press=()=>{store.save('merk',rowData.nama)
                                                                    this.props.navigation.goBack()}}>
                    <Card transparent>
                      <CardItem style={{borderRadius:26}}>
                          <Body style={{paddingLeft:10}}>
                            <Text style={styles.Text2}>{rowData.nama}</Text>
                          </Body>
                      </CardItem>
                    </Card>
                  </TouchableOpacity>
                  }>
        </ListView> 
        </Content>
      </Container>   
    );
  }
}
const styles = StyleSheet.create({
  container:{
    display:'flex',
    justifyContent:'center'
  },
  cat:{
    backgroundColor:'blue',
    padding:10,margin:10,width:'95%'
  },
  pageName:{
    margin:10,fontWeight:'bold',
    color:'#000', textAlign:'center'
  },
  btnText:{
    color:'#fff',fontWeight:'bold'
  },
   icon: {
    width: 24,
    height: 24,
    },
    image: {
    width: 30,
    height: 45,
    },
    spinner:{
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },

    miniimage:{
      width:100,height:90,
    },

    miniimage2:{
      width:150,height:70,
    },

    minitext2:{
      width:150,height:25,textAlign:'center',color:'white',
    },

    minitext:{
      width:100,height:40,textAlign:'center',color:'white',
    },

    miniview:{
      backgroundColor:'#F08080',margin:10,
    },
    Text:{
      fontFamily: "quicksand",
      fontStyle: "normal",
      fontWeight: "normal",
      fontSize: 18,
      textAlign: "center",
      color: "#E0E0E0",
      textShadowColor: "0px 4px 4px rgba(0, 0, 0, 0.25)"
    },
    Text:{textAlignVertical:'center',fontSize:20,color: '#E0E0E0',paddingVertical:5,paddingLeft: 10,fontWeight: "normal",alignItems: 'flex-end',fontFamily:'Quicksand-Bold'}
});


AppRegistry.registerComponent('merk_mobil', () => merk_mobil);
