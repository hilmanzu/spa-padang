import React, { Component } from 'react'
import MapView, {Marker} from 'react-native-maps'
import { View, StyleSheet,Dimensions,TouchableOpacity } from 'react-native'
import { Container, Header, Content, Item,Separator,Thumbnail,Text, Input,Button, Icon ,Label,Card,CardItem,Left,List,Badge, ListItem,Form, Body, Right} from 'native-base';
import store from 'react-native-simple-store';

const {width,height} = Dimensions.get('window')
const SCREEN_HEIGHT = height
const SCREEN_WIDTH = width
const ASPECT_RATIO = width/height
const LATITUDE_DELTA = 0.0922
const LONGITUDE_DELTA = LATITUDE_DELTA*ASPECT_RATIO

export default class maps extends Component {
  constructor(props){
    super(props)
    this.state = {
      initialPosition:{
        latitude: 0,
        longitude: 0,
        latitudeDelta: 0,
        longitudeDelta: 0,
      },
      markerPosition:{
        latitude: 0,
        longitude: 0,
      }
    }
  }

  watchID: ?number = null
  componentDidMount(){
    navigator.geolocation.getCurrentPosition((position)=>{
      var lat = parseFloat(position.coords.latitude)
      var long = parseFloat(position.coords.longitude)
      var initialRegion = {
        latitude: lat,
        longitude:long,
        latitudeDelta : LATITUDE_DELTA,
        longitudeDelta: LONGITUDE_DELTA,
      }
      this.setState({initialRegion:initialRegion})
      this.setState({markerPosition:initialRegion})

    }, 
    // (error)=> alert(JSON.stringify(error)),
    {enableHighAccuracy:true,timeout:20000,maximumAge:1000})
    this.watchID = navigator.geolocation.watchPosition((position)=>{
      var lat = parseFloat(position.coords.latitude)
      var long = parseFloat(position.coords.longitude)
      var lastRegion = {
        latitude: lat,
        longitude: long,
        longitudeDelta:LONGITUDE_DELTA,
        latitudeDelta: LATITUDE_DELTA,
      }
      this.setState({initialPosition: lastRegion})
      this.setState({markerPosition:lastRegion})
    })
  }
  componentWillUnmount(){
    navigator.geolocation.clearWatch(this.watchID)
  }

  render () {
    return (
      <View style={styles.container}>
        <MapView
          style={styles.map}
          region={this.state.initialPosition}
          showsUserLocation={true}>
        </MapView>
        <TouchableOpacity
              style={styles.btn}
              onPress={this.set}>
              <Text style={styles.btnText}> Set Lokasi </Text>
        </TouchableOpacity>
      </View>
    )   
  }
  set=()=>{
    const { navigate } = this.props.navigation;
    store.save('latitude',this.state.initialPosition.latitude)
    store.save('longitude',this.state.initialPosition.longitude)
    store.save('ok','ok')
    this.props.navigation.goBack()
  }
}
const styles = StyleSheet.create({
  container: { ... StyleSheet.absoluteFillObject },
  map: { ...StyleSheet.absoluteFillObject },
  btn:{
    borderRadius:20,
    alignSelf:'stretch',
    backgroundColor:'#e3f2fd',
    padding:20,
    alignItems:'center',
    margin:30
  },
})
