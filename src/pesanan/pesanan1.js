import React, { Component } from 'react';
import { Alert,BackHandle,AppRegistry,BackHandler,View,StyleSheet,ListView,Image,ImageBackground,TouchableOpacity } from 'react-native';
import { StackNavigator,DrawerNavigator,TabNavigator,TabBarBottom  } from 'react-navigation';
import { Container, Header, Content, Item,Separator,Thumbnail,Text, Input,Button, Icon ,Label,Card,CardItem,Left,List,Badge, ListItem,Form, Body, Right} from 'native-base';
import Carousel from 'react-native-banner-carousel';
import store from 'react-native-simple-store';
var Spinner = require('react-native-spinkit');
var last = require('array-last');
import firebase from 'firebase';
import moment from 'moment'

export default class pesanan1 extends Component {
   static navigationOptions = {
    headerStyle: {
          position: 'absolute',
          top: 0,
          left: 0
        },
      tabBarLabel: 'Profile',
      tabBarIcon: ({ tintColor }) => (
        <Image
          source={require('./image/Profile.png')}
          style={[styles.icon, { tintColor: tintColor }]}
        />
      ),
    };

constructor(props) {
  super(props)
  this.handleBackButtonClick = this.handleBackButtonClick.bind(this);
    this.state = {
      name: '',
      alamat: '',
      nohp:'',
      merk:'',
      jenis:'',
      lokasi:'',
      latitude:'',
      longitude:'',
      ok:'',
      hil:'',
      beli: new ListView.DataSource({rowHasChanged:(r1,r2)=> r1!=r2}),
      harga: new ListView.DataSource({rowHasChanged:(r1,r2)=> r1!=r2}),
      loading: true
    }
}

  componentDidMount(){
  console.reportErrorsAsExceptions = false;
  store.get('name').then((res) =>{this.setState({ name: res })})
  store.get('alamat').then((res) =>{this.setState({ alamat: res })})
  store.get('nohp').then((res) =>{this.setState({ nohp: res })})
  store.get('merk').then((res) =>{this.setState({ merk: res })})
  store.get('jenis').then((res) =>{this.setState({ jenis: res })})
  store.get('latitude').then((res) =>{this.setState({ latitude: res })})
  store.get('longitude').then((res) =>{this.setState({ longitude: res })})
  store.get('ok').then((res) =>{this.setState({ ok: res })})
  store.get('beli').then((res) =>{
        data = res; // here we have all products data
        this.setState({
          beli: this.state.beli.cloneWithRows(data),
          harga: this.state.harga.cloneWithRows(data),
          hil: res,loading:false
        })
      })
  ,this.timer = setTimeout(() => this.componentDidMount(), 2000)
  }
  

  componentWillMount() {
  BackHandler.addEventListener('hardwareBackPress', this.handleBackButtonClick);}

  componentWillUnmount() {
  BackHandler.removeEventListener('hardwareBackPress', this.handleBackButtonClick);
  }

  handleBackButtonClick() {
    Alert.alert(
       'Keluar dari pesanan',
       'semua pesanan akan dibatalkan anda yakin ?',
       [
         {text: 'Tidak', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
         {text: 'Iya', onPress: () => {
                  this.props.navigation.goBack()
                  store.delete('merk')
                  store.delete('jenis')
                  store.delete('latitude')
                  store.delete('longitude')
                  store.delete('ukuran')
                  store.delete('ok')
                  store.delete('beli')}
                  },
       ],
       { cancelable: false })

  return true;
  }


  render() {
    const {hil,beli,ukuran,lokasi,jenis,name,alamat,nohp,merk, loading,latitude,longitude,ok} = this.state;
    var total = 0

    if (loading === true) {
        return(
        <View style={{flex: 1,justifyContent: 'center',alignItems: 'center',backgroundColor:'#E0E0E0'}}>
          <Spinner style={styles.spinner} isVisible={true} type='Bounce' style={{width:50,height:50,color:'#bdbdbd'}}/>
          <Text style={{paddingTop:20}}> Loading </Text>
        </View>
        )
      }
    

    return (
      <Container>
        <Header style={{backgroundColor:'#64b5f6'}}>
          <Body><Text style={{color:'#ffffff'}}>Pesanan anda</Text></Body>
        </Header>
      <Content padder>
           <List>
             <Separator bordered>
             <Text>Nama</Text>
             </Separator>
            <ListItem>
              <Left>
                <Text placeholder="Underline Textbox">{name}</Text>
              </Left>
            </ListItem>

            <Separator bordered>
             <Text>Alamat</Text>
             </Separator>
            <ListItem>
              <Left>
                <Text>{alamat}</Text>
              </Left>
            </ListItem>

            <Separator bordered>
             <Text>No. Ponsel</Text>
             </Separator>
            <ListItem>
              <Left>
                <Text>{nohp}</Text>
              </Left>
            </ListItem>

            <Separator bordered>
             <Text>Merk Mobil</Text>
             </Separator>
            <ListItem onPress={this.merk}>
              <Left>
                <Text>{merk}</Text>
              </Left>
              <Right selected>
                <Icon name="arrow-forward" />
              </Right>
            </ListItem>

            <Separator bordered>
             <Text>Nama Mobil</Text>
             </Separator>
            <ListItem onPress={this.jenis}>
              <Left>
                <Text>{jenis}</Text>
              </Left>
              <Right selected>
                <Icon name="arrow-forward" />
              </Right>
            </ListItem>

            <Separator bordered>
             <Text>Set posisi lokasi anda</Text>
             </Separator>
            <ListItem onPress={this.lokasi}>
              <Left>
                <Text>set lokasi : {ok}</Text>
              </Left>
              <Right selected>
                <Icon name="arrow-forward" />
              </Right>
            </ListItem>

            <Separator bordered>
             <Text>Pilihan Paket Cucian</Text>
             </Separator>
               <ListItem onPress={this.paket}>
                <Left>
                  <Text>Tambahkan Paket Pencucian</Text>
                </Left>
               </ListItem>
            <Content>
              <ListView
                  horizontal={false}
                  dataSource={this.state.beli}
                  renderRow={(rowData,rowID,sectionID)=>
                    <Card transparent>
                      <CardItem style={{borderRadius:26}}>
                          <Body style={{paddingLeft:10}}>
                            <Text style={styles.Text2}>{rowData.name}{rowData.harga}</Text>
                            <Text style={styles.Text2}>{rowData.name}{rowData.harga}</Text>
                          </Body>
                          <Right>
                      <TouchableOpacity 
                        onPress={
                          handleBackButtonClick=()=>{
                            Alert.alert(
                             'Hapus',
                             'Yakin Ingin Menghapus ?',
                             [
                               {text: 'Tidak', onPress: () => console.log('Cancel Pressed'), style: 'cancel'},
                               {text: 'Iya', onPress:() =>{
                                                      store.get('beli')
                                                      .then((beli) =>
                                                      {beli.splice(sectionID,1)
                                                       store.save('beli', beli)})
                                                  }}
                             ],
                             { cancelable: false })
                        return true;
                        }}>
                          <Image style={{width:20,height:20}} source={require('./image/buang.png')}/>
                    </TouchableOpacity>
                </Right>
                      </CardItem>
                    </Card>
                  }>
              </ListView>
             </Content>

            <Separator>
              <Text>Total Harga </Text>
             </Separator>
               {hil.map((item) => <Text>{total=total + last(item.harga)}</Text>)}
            </List>

            <TouchableOpacity
              style={styles.btn}
              onPress={this.lanjut}>
              <Text style={styles.btnText}> Lanjut </Text>
            </TouchableOpacity>
          </Content>
      </Container>
    );
  }
  merk=()=>{
    const { navigate } = this.props.navigation;
    this.props.navigation.navigate('merk')
  }

  jenis=()=>{
    const { navigate } = this.props.navigation;
    this.props.navigation.navigate('jenis')
  }

  lokasi=()=>{
    const { navigate } = this.props.navigation;
    this.props.navigation.navigate('maps')
  }
  paket=()=>{
    const { navigate } = this.props.navigation;
    this.props.navigation.navigate('paket')
  }

  lanjut=()=>{
    const { navigate } = this.props.navigation;
    store.get('uid').then((res) =>{
    firebase.database().ref().child('user').child(res).child('pemesanan').set(
            {
                "name" : this.state.name,
                "alamat": this.state.alamat,
                "nohp":this.state.nohp,
                "merk":this.state.merk,
                "jenis":this.state.jenis,
                "lokasi":this.state.lokasi,
                "latitude":this.state.latitude,
                "longitude":this.state.longitude,
                "total":this.state.total,
              }
          )
    firebase.database().ref().child('pemesanan').push(
            {
                "name" : this.state.name,
                "alamat": this.state.alamat,
                "nohp":this.state.nohp,
                "merk":this.state.merk,
                "jenis":this.state.jenis,
                "lokasi":this.state.lokasi,
                "latitude":this.state.latitude,
                "longitude":this.state.longitude,
                "total":this.state.total,
              }
          )
    })
  Alert.alert('Terima Kasih','Pesanan anda sedang di proses oleh admin')
  this.props.navigation.goBack()
  store.delete('merk')
  store.delete('jenis')
  store.delete('latitude')
  store.delete('longitude')
  store.delete('ok')
  store.delete('beli')
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  icon: {
    width:20,height:20
  },
  btn:{
    borderRadius:20,
    alignSelf:'stretch',
    backgroundColor:'#e3f2fd',
    padding:20,
    alignItems:'center',
    margin:30
  },
  btnText:{
    color:'black',fontFamily: "quicksand",fontStyle: "normal",fontWeight: "normal",
  },
});
