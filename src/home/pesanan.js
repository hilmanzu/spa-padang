import React, { Component } from 'react';
import { AppRegistry,BackHandler,View,StyleSheet,ListView,Image,ImageBackground,TouchableOpacity } from 'react-native';
import { StackNavigator,DrawerNavigator,TabNavigator,TabBarBottom  } from 'react-navigation';
import { Container, Header,Fab, Content, Item,Separator,Thumbnail,Text, Input,Button, Icon ,Label,Card,CardItem,Left,List,Badge, ListItem,Form, Body, Right} from 'native-base';
import Carousel from 'react-native-banner-carousel';
import store from 'react-native-simple-store';
var Spinner = require('react-native-spinkit');
import firebase from 'firebase';
import moment from 'moment'

export default class pesanan extends Component {
   static navigationOptions = {
    headerStyle: {
          position: 'absolute',
          top: 0,
          left: 0
        },
      tabBarLabel: 'Pesanan',
      tabBarIcon: ({ tintColor }) => (
        <Image
          source={require('./image/pesanan.png')}
          style={[styles.icon, { tintColor: tintColor }]}
        />
      ),
    };

  render() {
    return (
      <Container>
        <Header style={{backgroundColor:'#64b5f6'}}>
          <Body><Text style={{color:'#ffffff'}}>Pesanan</Text></Body>
        </Header>
        <Content>
        </Content>
          <Fab 
            style={{ backgroundColor: '#5067FF' }}
            position="bottomRight"
            onPress={this.buy}>
            <Icon name="ios-add" />
          </Fab>
      </Container>
    );
  }
  buy =()=>{
    this.props.navigation.navigate('pesana')
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  icon: {
    width:20,height:20
  },
});
