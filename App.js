import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View
} from 'react-native';
import { createStackNavigator,DrawerNavigator,createTabNavigator,TabBarBottom  } from 'react-navigation';
///////////////auth///////////////////
import login from './src/auth/login';
import register from './src/auth/register';
// import splashscreen from './src/splashscreen';
// ///////tab////////////////////////////
import home from './src/home/home';
import pesanan from './src/home/pesanan';
import favorit from './src/home/favorite';
import bantuan from './src/home/bantuan';
import profile from './src/home/profile';
import pesanan1 from './src/pesanan/pesanan1';
///////pesanan///////////////////////////
import jenis_mobil from './src/pesanan/jenis_mobil';
import merk_mobil from './src/pesanan/merk_mobil';
import maps from './src/pesanan/maps';
import paket from './src/pesanan/paket';
// import selesai from './src/pesanan/selesai';
// ///////track/////////////////////////
// import track from './src/track/track';
// import menunggu from './src/track/menunggu';
// import pengiriman from './src/track/pengiriman';
// import finish from './src/track/finish';
// import verifikasi from './src/track/verifikasi';
// import tolak from './src/track/tolak';

const myApp = createTabNavigator({
  Home      : { screen: home },
  Pesanan   : { screen: pesanan},
  Favorit   : { screen: favorit},
  Bantuan    : { screen: bantuan},
  Profile   : { screen: profile},
},

{
    tabBarComponent: TabBarBottom,
    navigationOptions:{ header:{ visible:false }},
    lazy:true,
    tabBarPosition: 'bottom',
    animationEnabled: true,
    swipeEnabled: true,
    tabBackgroundColor : '#bbdefb',
    tabBarTextFontFamily: 'quicksand',
    tabBarOptions: {
      activeTintColor: '#0d47a1',
      inactiveTintColor: 'black',
      showIcon: true,
      showLabel: true,
    },
},{ headerMode: 'screen' });

const App = createStackNavigator({
  login       : { screen: login},
  register    : { screen: register},
  myApp 	    : { screen: myApp},
  merk        : { screen: merk_mobil},
  jenis       : { screen: jenis_mobil},
  pesana     : { screen: pesanan1},
  maps        : { screen:maps},
  paket       : { screen:paket},

},{headerMode: 'none'});




export default App;