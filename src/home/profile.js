import React, { Component } from 'react';
import { AppRegistry,BackHandler,View,StyleSheet,ListView,Image,ImageBackground,TouchableOpacity } from 'react-native';
import { StackNavigator,DrawerNavigator,TabNavigator,TabBarBottom  } from 'react-navigation';
import { Container, Header, Content, Item,Separator,Thumbnail,Text, Input,Button, Icon ,Label,Card,CardItem,Left,List,Badge, ListItem,Form, Body, Right} from 'native-base';
import Carousel from 'react-native-banner-carousel';
import store from 'react-native-simple-store';
var Spinner = require('react-native-spinkit');
import firebase from 'firebase';
import moment from 'moment'

export default class profile extends Component {
   static navigationOptions = {
    headerStyle: {
          position: 'absolute',
          top: 0,
          left: 0
        },
      tabBarLabel: 'Profile',
      tabBarIcon: ({ tintColor }) => (
        <Image
          source={require('./image/Profile.png')}
          style={[styles.icon, { tintColor: tintColor }]}
        />
      ),
    };

constructor(props) {
  super(props)
    this.state = {
      dataSource: '',
      loading: true
    }
}

componentDidMount(){
      const { navigate } = this.props.navigation;
      store.get('uid').then((res) =>{
        console.log(res)
      fetch('https://spa-padang.firebaseio.com/user/' + res + '.json?print=pretty')
        .then((response) => response.json())
        .then((responseJson) =>{
          store.save('name',responseJson.username)
          this.setState({ dataSource: responseJson, loading: false }) 
          })
        })
  }
  

  render() {
    const { dataSource, loading} = this.state;
    return (
      <Container>
      <Content padder>
            <View style={{justifyContent:'center',alignItems:'center',marginTop:40,}}>
              <Image style={{width:100,height:100}} source={require('./image/user.png')}/>
           </View>
            <Card transparent style={{padding:30,borderRadius:20,marginTop:20}}>
              <Item>
                <Text style={styles.Text}> Nama Lengkap</Text>
                <Text style={styles.Text}> {dataSource.nama_lengkap}</Text>
              </Item>

              <Item>
                <Text style={styles.Text}> username </Text>
                <Text style={styles.Text}> {dataSource.username} </Text>
              </Item>

              <Item>
                <Text style={styles.Text}> Email </Text>
                <Text style={styles.Text}> {dataSource.email} </Text>
              </Item>

              <Item>
                <Text style={styles.Text}> Phone </Text>
                <Text style={styles.Text}> {dataSource.nohp} </Text>
              </Item>

              <Item>
                <Text style={styles.Text}> Alamat </Text>
                <Text style={styles.Text}> {dataSource.alamat} </Text>
              </Item>

            </Card>
            <TouchableOpacity
              style={styles.btn}
              onPress={this.logout}>
              <Text style={styles.btnText}> KELUAR </Text>
            </TouchableOpacity>
          </Content>
      </Container>
    );
  }
  logout=()=>{
    const { navigate } = this.props.navigation;
    store.delete('uid');
    this.props.navigation.navigate('login')
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  icon: {
    width:20,height:20
  },
  btn:{
    borderRadius:20,
    alignSelf:'stretch',
    backgroundColor:'#e3f2fd',
    padding:20,
    alignItems:'center',
    margin:30
  },
  btnText:{
    color:'black',fontFamily: "quicksand",fontStyle: "normal",fontWeight: "normal",
  },
});
