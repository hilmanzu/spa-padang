import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  View,
  TextInput,
  TouchableOpacity,
  ImageBackground,
  Image,
  Alert,
} from 'react-native';
import { Container,Text, Header,Button, Content, Item, Input, Icon ,Label,Card,Left,List, ListItem,Form, Body, Right} from 'native-base';
import firebase from 'firebase';
import store from 'react-native-simple-store';


export default class register extends Component{
  static navigationOptions = {
      headerStyle: {
        position: 'absolute',
        top: 0,
        left: 0
      },
      headerBackTitleStyle: {
          opacity: 0,
      },
      headerTintColor: '#fff'
  };

  constructor(props){
  super(props);
  this.state = {
    email:'',
    password:'',
    alamat:'',
    username:'',
    nohp:'',
    loading: true,
    key:true,
    namalengkap:'',
  }
}

// componentDidMount(){
//   // const { navigate } = this.props.navigation;
//   store.get('email').then((email) =>{
//     console.log(email)
//   if(email){
//     // this.props.navigation.navigate('Home')}
//   })
// }

  render() {
      return (
      <Container >
        <Header style={{backgroundColor:'#64b5f6'}}>
          <Body><Text style={{color:'#ffffff'}}>Daftar</Text></Body>
        </Header>
      <Content style={{paddingLeft:60,paddingRight:60,paddingTop:40}}>
            <Item floatingLabel style={{marginBottom:20,backgroundColor:'#ffffff'}}>
              <Label> Username </Label>
              <Input
                style={{color:'#424242'}}     
                onChangeText={(username)=> this.setState({username})} 
                returnKeyType='next'
                underlineColorAndroid='transparent'/>
            </Item>

            <Item floatingLabel style={{marginBottom:20,backgroundColor:'#ffffff'}}>
              <Label> Nama lengkap</Label>
              <Input
                style={{color:'#424242'}}     
                onChangeText={(namalengkap)=> this.setState({namalengkap})} 
                returnKeyType='next'
                underlineColorAndroid='transparent'/>
            </Item>

            <Item floatingLabel style={{marginBottom:20,backgroundColor:'#ffffff'}} >
              <Label> Email </Label>
                <Input
                  style={{color:'#424242'}}  
                  onChangeText={(email)=> this.setState({email})} 
                  returnKeyType='next'
                  underlineColorAndroid='transparent'/>
              </Item>

              <Item floatingLabel style={{marginBottom:20,backgroundColor:'#ffffff95'}} >
                <Label> Password </Label>
                <Input
                  style={{color:'#424242'}}  
                  onChangeText={(password)=> this.setState({password})} 
                  returnKeyType='next'
                  secureTextEntry={this.state.key}
                  underlineColorAndroid='transparent'/>
                  <TouchableOpacity onPressIn={ () => this.key() } onPressOut={ () => this.key1() } >
                    <Image style={{width:20,height:20,marginRight:10}} source={require('./image/eye.png')}/>
                  </TouchableOpacity>
              </Item>

              <Item floatingLabel style={{marginBottom:20,backgroundColor:'#ffffff95'}} >
                <Label> Phone </Label>
                <Input
                  style={{color:'#424242'}}  
                  onChangeText={(nohp)=> this.setState({nohp})} 
                  returnKeyType='next'
                  underlineColorAndroid='transparent'/>
              </Item>

              <Item floatingLabel style={{marginBottom:20,backgroundColor:'#ffffff95'}} >
                <Label> Alamat </Label>
                <Input
                  style={{color:'#424242'}}  
                  onChangeText={(alamat)=> this.setState({alamat})} 
                  returnKeyType='next'
                  underlineColorAndroid='transparent'/>
              </Item>
              
          <TouchableOpacity
            style={styles.btn}
            onPress={this.register}>
            <Text style={{color:'#424242',fontSize:15,fontWeight:'bold',}}> DAFTAR </Text>
          </TouchableOpacity>
          
          <TouchableOpacity
            style={styles.btn}
            onPress={this.kembali}>
            <Text style={{color:'#424242',fontSize:15,fontWeight:'bold',}}> KEMBALI </Text>
          </TouchableOpacity>
          </Content>
        </Container>
      )
    }

    key = ()=>{
    this.setState({ 
      key:false ,
    })}

    key1 = ()=>{
    this.setState({ 
      key:true ,
    })} 

    kembali = ()=>{
      this.props.navigation.navigate('login');
    }

    register = () => {
      const { email, password } = this.state;
      firebase.auth().createUserWithEmailAndPassword(email, password)
        .then((response) => {
          console.log(response)
          if (response.uid){
          this.props.navigation.navigate('login');
          firebase.database().ref().child('user').child(response.uid).set(
            {
                "username" : this.state.username,
                "email"    : this.state.email,                
                "password" : this.state.password,
                "nohp"     : this.state.nohp,
                "alamat"   : this.state.alamat,
                "nama_lengkap"   : this.state.namalengkap,
              }
          )
          firebase.database().ref().child('user_list').push(
            {
                "username" : this.state.username,
                "email"    : this.state.email,                
                "password" : this.state.password,
                "nohp"     : this.state.nohp,
                "alamat"   : this.state.alamat,
                "nama_lengkap"   : this.state.namalengkap,
                "uid"      : response.uid
              }
          )
           Alert.alert(
            'Selamat anda telah terdaftar',
            'silahkan login untuk melihat akun anda'
          )
          }
        })
        .catch((error) => {
         Alert.alert(
            'Tidak dapat membuat akun',
            'Cek kembali email dan password anda'
          )
        });
    }
  }

  const styles = StyleSheet.create({
  wrapper:{
    flex:1,
  },
  container:{
    flex:1,
    alignItems:'center',
    justifyContent:'center',
    paddingLeft:60,
    paddingRight:60,
    backgroundColor: '#ffffff',
  },
  header:{
    fontSize:40,
    marginVertical:60,
    color:'#424242',
    fontWeight:'bold',
    textAlign:'center'
  },
  textInput:{
    alignSelf: 'stretch',
    padding:16,
    marginBottom:20,
    backgroundColor:'#ffffff99',
    borderRadius:20,
  },
  btn:{
    alignSelf:'stretch',
    backgroundColor:'#e3f2fd',
    padding:15,
    alignItems:'center',
    borderRadius:10,
    marginVertical: 5,
  }
})
